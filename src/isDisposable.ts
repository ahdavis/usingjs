/*
 * isDisposable.ts
 * Defines a typeguard for checking if an object is disposable
 * Created on 9/19/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * Licensed under the MIT License
 */

//import
import { Disposable } from './Disposable';

/**
 * Checks that an object implements the
 * [[Disposable]] interface
 *
 * @param obj The object to typecheck
 *
 * @returns Whether `obj` is disposable
 */
export function isDisposable(obj: any): obj is Disposable {
	return 'dispose' in obj;
}

//end of file
