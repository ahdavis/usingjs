/*
 * using.ts
 * Defines a function that manages the lifetime of an object
 * Created on 9/19/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * Licensed under the MIT License
 */

//imports
import { Disposable } from './Disposable';
import { isDisposable } from './isDisposable';
import { UsingCallback } from './UsingCallback';
import { UsingError } from './UsingError';

/**
 * Manages the lifetime of a disposable object
 *
 * @param obj The object to manage
 * @param func The function that `obj` is valid inside
 *
 * @throws UsingError if obj doesn't implement the [[Disposable]] interface
 */
export function using<T extends Disposable>(obj: T, 
						func: UsingCallback<T>) {
	//ensure that the object implements the Disposable interface
	if(!isDisposable(obj)) {
		throw new UsingError();
	}

	//execute the usage function and clean up the object
	try {
		//call the usage function
		func(obj);
	} finally {
		//and 
		obj.dispose();
	}
}

//end of file
