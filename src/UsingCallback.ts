/*
 * UsingCallback.ts
 * Defines a type for the callback of the using function
 * Created on 9/19/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * Licensed under the MIT License
 */

//import
import { Disposable } from './Disposable';

/**
 * The callback function for a [[using]] function call
 */
export type UsingCallback<T extends Disposable> = (obj: T) => void;

//end of file
