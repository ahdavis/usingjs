/*
 * Disposable.ts
 * Defines the interface of a usage-managed object
 * Created on 9/19/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * Licensed under the MIT License
 */

//no imports

/**
 * Implemented by usage-managed objects
 */
export interface Disposable {
	/**
	 * Called when the managed object
	 * reaches the end of its lifetime
	 */
	dispose(): void;
}

//end of file
