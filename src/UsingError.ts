/*
 * UsingError.ts
 * Defines an error to be thrown if a non-disposable object is
 * passed to a using function call
 * Created on 9/19/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * Licensed under the MIT License
 */

//no imports

/**
 * Called when an invalid object is passed
 * to the [[using]] function
 */
export class UsingError extends Error {
	//no fields
	
	/**
	 * Constructs a new `UsingError` instance
	 */
	constructor() {
		//call the superclass constructor
		super('Non-disposable object passed to using() function');
	}
}

//end of file
