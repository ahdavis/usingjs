/*
 * isDisposable.test.ts
 * Defines unit tests for the isDisposable function
 * Created on 9/19/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * Licensed under the MIT License
 */

//imports
import { Disposable } from '../src/Disposable';
import { isDisposable } from '../src/isDisposable';

//define two classes for testing the function against
class Foo implements Disposable {
	public dispose(): void {
		//do nothing
	}
}
class Bar {
	//empty class
}

//this test checks that the Foo class is disposable
test('Object is disposable', () => {
	expect(isDisposable(new Foo())).toBeTruthy();
});

//this test checks that the Bar class is not disposable
test('Object is not disposable', () => {
	expect(isDisposable(new Bar())).toBeFalsy();
});

//end of file
