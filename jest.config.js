/*
 * jest.config.js
 * Jest config file
 * Created on 9/19/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * Licensed under the MIT License
 */

//no imports

//export the config options
module.exports = {
	testRegex: "./tests/.*\\.(ts)$",
	transform:  {
		"^.+\\.tsx?$": "ts-jest"
	}
}

//end of file
