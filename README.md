# using.js

Adds a `using` statement to TS or JS


# About

This package adds scoped object lifetimes to JavaScript or TypeScript.
This is achieved through a single function, `using`, that accepts an
object and a callback function that takes that object as an argument.
After that callback function completely runs, regardless of errors thrown
during that time, the object will be disposed of. For this to happen,
however, the object must contain a method called `dispose` that takes no
arguments and returns nothing. If you are using TypeScript, this is
enforced by ensuring that the object passed to `using` implements
the `Disposable` interface exposed by this package. If you are using
JavaScript, however, an error will be thrown if the object does not
contain a `dispose` method.

# Installation

To install this package, type

`npm install using` 

or

`yarn add using`

at the command line of your computer.

# API

This package exposes three modules. They are:

* `Disposable`, an interface that provides the mentioned `dispose` method
* `using`, the function mentioned above
* `UsingError`, an error that is thrown if an object is not `Disposable`.

The declarations of these modules are as follows:

```typescript
interface Disposable {
	dispose(): void;
}

function using<T extends Disposable>(obj: T, func: (obj: T) => void);

class UsingError extends Error {
	//constructor omitted
}

```

# License

This package is licensed under the MIT License. See the `LICENSE` file
in its GitLab repository for more information.
